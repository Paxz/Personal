﻿#Needed to fix Problem with the webquery on the https page
add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy


#Queries
$newestUrl = "https://loadingartist.com/comic/red-alert/"
#The page of the first comic doesn't have a <back> comic-entry, thats why the images are in another order.
#Thats why we start at the second Comic
$url = "https://loadingartist.com/comic/dreams/"

do{
    $invoke = Invoke-WebRequest -Uri $url -UseBasicParsing
    $ImageUrl = $("https://loadingartist.com" + $invoke.Images.src[1])
    $time = $ImageUrl.split('/') | Select-Object -last 2 -Skip 1
    $folder = "$PSScriptRoot\$($time[0])\$($time[1])"
    if(!(Test-Path -Path $folder))
    {
        mkdir $folder
    }
    Invoke-WebRequest -Uri $ImageUrl -UseBasicParsing -Outfile $folder\$($invoke.Images.title[0]).png
    $url = $invoke.Links | Where-Object {$_.class -eq "normal highlight next comic-thumb"} | Select-Object -ExpandProperty href
}while ($url -ne $newestUrl)