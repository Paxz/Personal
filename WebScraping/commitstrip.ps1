﻿$startUrl = "http://www.commitstrip.com/en/2012/02/22/interview/"
$newUrl = $startUrl

$dontStop = $true

while($dontStop)
{
    $newComicPage = Invoke-WebRequest -Uri $newUrl
    if ($newComicPage.Links.rel -contains "next")
    {
        $newUrl = $newComicPage.Links | Where-Object {$_.rel -eq "next"} | Select-Object -ExpandProperty href -First 1
    }
    else
    {
        $dontStop = $false
    }

    $comicTitle = $newComicPage.AllElements | Where-Object {$_.class -eq "entry-title"} | Select-Object -ExpandProperty innerText
    $comicTitle = $comicTitle -replace '[^\w*\d* *]',''

    $foldername = $newComicPage.AllElements | Where-Object {$_.class -eq "entry-date"} | Select-Object -ExpandProperty dateTime
    $folderyear = $foldername.Substring(0,4)
    $foldermonth = $foldername.Substring(0,7)
    if(!(Test-Path $PSScriptRoot\$folderyear\$foldermonth))
    {
        mkdir $PSScriptRoot\$folderyear\$foldermonth
    }

    #Download Image
    Write-Host "Downloading $comicTitle from $foldermonth"
    Invoke-WebRequest -Uri ($newComicPage.Images.src[0]) -OutFile $PSScriptRoot\$folderyear\$foldermonth\$comicTitle.jpeg
    Start-Sleep -Seconds 1
}
