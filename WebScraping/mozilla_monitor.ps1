﻿param(
    $emails
)

#Generating SHA1 Hash from Email
#Ref.: https://gallery.technet.microsoft.com/scriptcenter/Get-StringHash-aa843f71
Function Get-StringHash([String] $email,$HashName = "SHA1") 
{ 
	$StringBuilder = New-Object System.Text.StringBuilder 
	[System.Security.Cryptography.HashAlgorithm]::Create($HashName).ComputeHash([System.Text.Encoding]::UTF8.GetBytes($email))|%{ [Void]$StringBuilder.Append($_.ToString("x2"))}
    $Hash = $StringBuilder.ToString()
    
    return $Hash
}

$scans = @()

foreach ($email in $emails)
{
    #Creaing Hash
    $emailHash = Get-StringHash -email $email -HashName "SHA1"

    #Webquery to monitor.firefox.com/scan for breaches related to the email
    $mozillaCheck = Invoke-WebRequest -Uri "https://monitor.firefox.com/scan" -Method "POST" -Headers @{"Cache-Control"="max-age=0"; "Origin"="https://monitor.firefox.com"; "Upgrade-Insecure-Requests"="1"; "User-Agent"="Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"; "Accept"="text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"; "Referer"="https://monitor.firefox.com/"; "Accept-Encoding"="gzip, deflate, br"; "Accept-Language"="de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7";} -ContentType "application/x-www-form-urlencoded" -Body "email=&emailHash=$emailHash"

    #Checking if the email is found in a breach 
    $mozillaResponse = $mozillaCheck | Select-Object -ExpandProperty AllElements | Where-Object {$_.class -eq "section-wrapper breach-container"}
    if($mozillaResponse.innerText -like "*Your account did not appear in our basic scan*")
    {
        Write-Host "$email did not appear in any breach." -ForegroundColor Green
    }
    else{
        Write-Host "$email did appear in a breach!" -ForegroundColor Red
        #Getting the names of each breach
        $mozillaResponse = $mozillaResponse | % {$_.innerText -split "`n" | Select-Object -Skip 3 | Select-Object -SkipLast 1}
        $singleScan = [ordered]@{Email=$email; Breaches=($mozillaResponse.trim() -ne "" | Where-Object {$_ -notlike "Breach*"})}
        
        #Adding the object with the email and a list of all breaches to the finally returned object
        $scans += New-Object -TypeName psobject -Property $singleScan
    }
}

return $scans