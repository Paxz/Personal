﻿$startDate = Get-Date -Year 1989 -Month 4 -Day 16
$endDate = Get-Date

while($startDate -le $endDate)
{
    if(!(Test-Path .\$($startDate.Year)\$($startDate.Month)))
    {
        mkdir -Path .\$($startDate.Year)\$($startDate.Month)
    }

    $stringDate = "$($startDate.Year)-$($startDate.Month)-$($startDate.Day)"
    Invoke-WebRequest -Uri ((Invoke-WebRequest -Uri "http://dilbert.com/strip/$stringDate" -UseBasicParsing -TimeoutSec 10).Images[3].src) -UseBasicParsing -TimeoutSec 5 -OutFile ".\$($startDate.Year)\$($startDate.Month)\$stringDate.gif"

    $outputString = "Downloaded Comic: $stringDate"
    Write-Host $outputString
    Out-File -FilePath .\log.txt -Append -InputObject $outputString

    $startDate = $startDate.AddDays(1)
    Start-Sleep -Seconds 1
}