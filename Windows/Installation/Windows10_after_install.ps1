function Disable-Apps
{
    #disables consumer-mode
    if(Test-Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent")
    {
        $tmp= (Get-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent")
        if($tmp.DisableWindowsConsumerFeatures -eq 0)
        {
            Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent" -Name DisableWindowsConsumerFeatures 1
            Write-Output "Consumer Modus is disabled now!"
        }
        else
        {
            Write-Output "Consumer Modus allready disabled!"
        }
    }
    else
    {
        Write-Host "Consumer Mode can't be deactivated for your version of Windows."
    }

    Write-Host "Uninstalling Applications..." -foregroundcolor Yellow

    #Uninstalls applications provided by Windows
    Get-AppxPackage -AllUsers *AdobePhotoshopExpress* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Candy* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Duolingo* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *EclipseManager* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *FarmVille* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.3DBuilder* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.BingNews* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.BingTranslator* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.BingWeather* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.FreshPaint* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.Getstarted* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.Messaging* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.MicrosoftOfficeHub* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.MicrosoftSolitaireCollection* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.NetworkSpeedTest* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.Office.OneNote* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.People* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.SkypeApp* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.WindowsAlarms* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.WindowsFeedbackHub* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.WindowsMaps* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.XboxApp* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.ZuneMusic* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Microsoft.ZuneVideo* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Netflix* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *PandoraMediaInc* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *PicsArt* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Twitter* | Remove-AppxPackage
    Get-AppxPackage -AllUsers *Wunderlist* | Remove-AppxPackage

    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*AdobePhotoshopExpress*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Candy*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Duolingo*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*EclipseManager*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*FarmVille*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.3DBuilder*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.BingNews*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.BingTranslator*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.BingWeather*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.FreshPaint*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.Getstarted*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.Messaging*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.MicrosoftOfficeHub*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.MicrosoftSolitaireCollection*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.NetworkSpeedTest*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.Office.OneNote*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.People*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.SkypeApp*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.WindowsAlarms*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.WindowsFeedbackHub*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.WindowsMaps*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.XboxApp*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.ZuneMusic*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Microsoft.ZuneVideo*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Netflix*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*PandoraMediaInc*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*PicsArt*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Twitter*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
    Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -Like "*Wunderlist*"} | ForEach-Object { Remove-AppxProvisionedPackage -Online -PackageName $_.PackageName}
}

function Install-Updates
{
    $Criteria = "IsInstalled=0 and Type='Software'"
    
    $Searcher = New-Object -ComObject Microsoft.Update.Searcher
    $SearchResult = $Searcher.Search($Criteria).Updates
    $Session = New-Object -ComObject Microsoft.Update.Session

    $Downloader = $Session.CreateUpdateDownloader()
    $Downloader.Updates = $SearchResult
    $Downloader.Download()

    $Installer = New-Object -ComObject Microsoft.Update.Installer
    $Installer.Updates = $SearchResult
    $Result = $Installer.Install()
    return $Result.rebootRequired
}

function Install-Software
{
    Param
    (
        $to_install
    )

    If(!(Test-Path ".\temp\"))
    {
        mkdir ".\temp"
    }
    
    for ($i=0;$i -ne ($to_install.Length -1);$i++)
    {
        if($to_install.Status[$i] -eq 1)
        {
            switch($to_install.Software[$i])
            {
                7zip {
                    #Downloads and installs 7zip
                    Invoke-WebRequest -Uri "http://www.7-zip.org/a/7z1805-x64.exe" -Outfile ".\temp\7zip.exe"
                    .\temp\7zip.exe /S
                }

                VSCode {
                    #Downloads and installs VS-Code
                    Invoke-WebRequest -Uri "http://go.microsoft.com/fwlink/?Linkid=852157" -OutFile ".\temp\vscode.exe"
                    .\temp\vscode.exe /verysilent /norestart
                }

                Notepadplusplus {
                    #Downloads and installs Notepad ++
                    Invoke-WebRequest -Uri "http://notepad-plus-plus.org/repository/7.x/7.5.6/npp.7.5.6.Installer.x64.exe" -OutFile ".\temp\npp.exe"
                    .\temp\npp.exe /S
                }

                Keepass {
                    #Downloads and installs Keepass
                    Invoke-WebRequest -Uri "http://sourceforge.net/projects/keepass/files/KeePass%202.x/2.39.1/KeePass-2.39.1-Setup.exe/download?" -OutFile ".\temp\keepass.exe"
                    .\temp\keepass.exe /silent
                }

                GitforWindows {
                    #Downloads and installs Git for Windows
                    Invoke-WebRequest -Uri "http://github.com/git-for-windows/git/releases/download/v2.17.0.windows.1/Git-2.17.0-64-bit.exe" -OutFile ".\temp\gitforwindows.exe"
                    .\temp\gitforwindows.exe /SILENT /LOADINF=".\git_install.txt"

                }

                Telegram {
                    #Downloads and installs Telegram
                    Invoke-WebRequest -Uri "http://telegram.org/dl/desktop/win" -OutFile ".\temp\telegram.exe"
                    .\temp\telegram.exe /silent
                }

                VLC {
                    #Downloads and installs VLC
                    Invoke-WebRequest -Uri "https://www.vlc.de/download/vlc/vlc-3.0.2-win64.exe" -OutFile ".\temp\vlc.exe"
                    .\temp\vlc.exe /L1033 /S
                }

                Firefox {
                    #Downloads and installs Firefox
                    Invoke-WebRequest -Uri "http://download.mozilla.org/?product=firefox-latest-ssl&os=win64&lang=de" -OutFile ".\temp\firefox.exe"
                    .\temp\firefox.exe /S
                }

                Java {
                    #Downloads and installs Java
                    Invoke-WebRequest -Uri "http://javadl.oracle.com/webapps/download/AutoDL?BundleId=227521_e758a0de34e24606bca991d704f6dcbf" -OutFile ".\temp\java.exe"
                    .\temp\java.exe /s AUTO_UPDATE=Enable
                }

                Steam {
                    #Downloads and installs Steam
                    Invoke-WebRequest -Uri "http://steamcdn-a.akamaihd.net/client/installer/SteamSetup.exe" -OutFile ".\temp\steam.exe"
                    .\temp\steam.exe /S
                }

                Chrome {
                    #Downloads and installs Chrome
                    Invoke-WebRequest -Uri "http://dl.google.com/chrome/install/375.127/chrome_installer.exe" -OutFile ".\temp\chrome.exe"
                    .\temp\chrome.exe /silent /install
                }

                default {
                    Write-Host "$($to_install.Software) konnte nicht gefunden werden."
                }
            }
        }
    }
}

function Set-Options
{
    #Disabling Telemetry
    Write-Host "Disabling Telemetry..."
    Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\DataCollection" -Name "AllowTelemetry" -Type DWord -Value 0
    
    # Stop and disable Diagnostics Tracking Service
    Write-Host "Stopping and disabling Diagnostics Tracking Service..."
    Stop-Service "DiagTrack"
    Set-Service "DiagTrack" -StartupType Disabled
    
    # Show known file extensions
    Write-Host "Showing known file extensions..."
    Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "HideFileExt" -Type DWord -Value 0
    
    #Disable People in taskbar
    Write-Host "Disabling People Icon in Taskbar"
    Set-ItemProperty -Path "Registry::HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People\" -Name "PeopleBand" -Value 0
}

function Send-Telegram
{
    #Sets Telegram APIkey and ChatId to Message User on Telegram later
    [string]$api = ""
    [string]$chatId = ""

    #Gets OSinformations
    $OSinfo= gcim Win32_OperatingSystem | Select-Object OSArchitecture, Caption

    #Checks for devices whose ConfigManagerErrorCode value is greater than 0, i.e has a problem device.
    [array]$result = (Get-WmiObject -Class Win32_PnpEntity -ComputerName localhost -Namespace Root\CIMV2 | Where-Object {$_.ConfigManagerErrorCode -gt 0 } | Select-Object Name, ConfigManagerErrorCode)
    if ($result.length -gt 0)
    {
        for($i = 0 ; $i -ne ($result.Length); $i++)
            {
                [string]$drivermsg += "$($i+1). $($result[$i].Name),$($result[$i].ConfigManagerErrorCode)`n"
            }
    }

    #Preparing Message
    [string]$message = "$($OSinfo.Caption), $($OSinfo.OSArchitecture) is now succesfully installed on $env:computername `n$drivermsg"

    #Write Message to Telegram User
    Invoke-RestMethod -Uri ('http://api.telegram.org/bot{0}/sendMessage?chat_id={1}&text={2}' -f $api, $chatId, $message) > $0
}

#Import config from config.csv
$config = @()
$config = Import-Csv .\config.csv -Delimiter ';'

Disable-Apps
$need_reboot = Install-Updates
Install-Software -to_install $config
Set-Options
Send-Telegram

Remove-Item -Recurse -Force .\temp

if($need_reboot)
{
    Restart-Computer
}