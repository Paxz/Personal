<#
	.SYNOPSIS
	Converts Boostnote Notes to different other formats using pandoc.
		
	.PARAMETER inPath
    Path of the .cson files.
	
	.PARAMETER outPath
    Path to save the converted files.
	
	.PARAMETER type
	Format to convert the files into.

	.INPUTS
    String.

    .OUTPUTS
	null. Returns nothing but the new files to the adressed folder.
	
	.EXAMPLE
	PS C:\>.\convert_boostnote.ps1 -inPath .\notes\ -outPath .\newFiles\ -type dokuwiki
    Converts all files from '.\notes\' and saves them in dokuwiki format in the folder '.\newFiles\'
    
	.LINK
	https://github.com/jgm/pandoc
	#>

[CmdletBinding()]
param
(
    # Parameter help description
    [Parameter(
        Position=0,
        Mandatory=$true,
        HelpMessage="Input can be a file or a folder.",
        ValueFromPipeline=$true)]
    [string]
    $inPath,

    [Parameter(
        Position=1,
        Mandatory=$false,
        HelpMessage="The output path needs to end with '\'.",
        ValueFromPipeline=$false)]
    [ValidatePattern(".*\\$")]
    [string]
    $outPath = ".\",

    [Parameter(
        Position=2,
        Mandatory=$true,
        HelpMessage="Press Tab to get all avaible formats.",
        ValueFromPipeline=$false)]
    [ValidateSet('markdown','html','dokuwiki')]
    [string]
    $type
)

begin
{
    Write-Verbose -Message "Getting Files of the Directory $inPath"
    $files = Get-ChildItem -Path $inPath -File | Select-Object -ExpandProperty FullName
    $tempfile = ".\convert-temp.txt"

    Write-Verbose -Message "Getting File-Extention from Type Parameter"
    switch($type)
    {
        markdown {
            $filetype = "md"
        }
        html {
            $filetype = "html"
        }
        dokuwiki {
            $filetype = "txt"
        }
    }
}


Process
{
    foreach ($file in $files)
    {
        Write-Verbose -Message "Getting Title of the Document: $file"
        $title = (Get-Content $file | Select-Object -First 1 -Skip 4) -split ('"') | Select-Object -First 1 -Skip 1
        
        Write-Verbose -Message "Write Content to temp file"
        Get-Content $file | Select-Object -Skip 6 | Select-Object -SkipLast 4 | Out-File -FilePath $tempfile -Encoding utf8
    
        Write-Debug "Temp-File created."
        Write-Verbose -Message "Running pandoc.exe"
        C:\Users\geuenich\AppData\Local\Pandoc\pandoc.exe -f markdown -t $type -o "$outPath$title.$filetype" $tempfile
    }
}

end
{
    Write-Verbose -Message "Deleting temporary file."
    Remove-Item $tempfile
}
