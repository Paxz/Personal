﻿#$json = Get-Content .\result.json | ConvertFrom-Json
#Show-Graph -Datapoints (Get-AverageTime -json $json -name "<name>" | Select-Object -ExpandProperty Count) -XAxisTitle "Hour" -YAxisTitle "Messages" -XAxisStep 6 -YAxisStep 300

function Get-AverageTime
{
    param
    (
        $name,
        $json
    )

    $time = @()
   
    $messages = $json.chats | Where-Object {$_.Name -eq $name} | Select-Object -ExpandProperty messages
    
    foreach ($message in $messages)
    {
        $time += [timespan]($message.date -split "T")[1]
    }
    
    $AverageTime = New-TimeSpan -Seconds ($time.TotalSeconds | Measure-Object -Average | Select-Object -ExpandProperty Average)
    
    $addedHours = $time.Hours | Group-Object | Sort-Object {[int]$_.Name} | Select-Object Name, Count

    return $addedHours
}

function Get-MessageNumbers
{
    param
    (
        $name,
        $json
    )

    $messages = $json.chats | Where-Object {$_.Name -eq $name} | Select-Object -ExpandProperty messages
    $messageNumbers = $messages.Count
    $messagesPerUser = $messages | Group-Object "From" | Select-Object Name, Count
}

