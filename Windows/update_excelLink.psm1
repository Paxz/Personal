function Update-ExcelLink
{
    <#
    .SYNOPSIS
    Ersetzt externe Links in .xslx Dateien per COM-Object

    .DESCRIPTION
    Nicht im gleichen Ordner laufen lassen wie die zu bearbeitenden Excel-Dateien. Zumindestens sollte man sonst den Filter fr Excel Dateien erweitern, sonst wird der Befehl auch fr die vorherigen Backup Dateien ausgefhrt.

    .PARAMETER filepath
    Kompletter Pfad zu einer oder mehreren Excel-Dateien.

    .PARAMETER searchPattern
    Suchverhalten nachdem die Links gefunden werden. (Abgleich mit -like , kein RegEx)

    .PARAMETER stringToReplace
    Der String der im Link ersetzt werden soll. (!Beachte! Sonderzeichen wie '\' müssen escaped werden mit einem weiteren '\'. '\\' ist also '\\\\')

    .PARAMETER replacement
    Der String der beim ersetzen eingefügt wird.

    .EXAMPLE
    PS C:\>Import-Module .\update_excelLink.psm1
    PS C:\>Update-ExcelLink -filepath "C:\excelfile.xlsx" -searchPattern "\\fileserver*" -stringToReplace "\\\\fileserver" -replacement "\\fileservertest" -Verbose

    .EXAMPLE
    PS C:\>Import-Module .\update_excelLink.psm1
    PS C:\>Get-ChildItem -Recurse .\folder -Filter *.xlsx | Select-Object -ExpandProperty Fullname | Update-ExcelLink -searchPattern "\\fileserver*" -stringToReplace "\\\\fileserver" -replacement "\\fileservertest" -Verbose
    Run the script for every xlsx file in a folder and its subfolder.

    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,
			ValueFromPipeline=$true,
			Position=0)]
        [ValidateNotNullOrEmpty()]
        [string[]]
        $filepath,
        
        [Parameter(Mandatory=$true,
			ValueFromPipeline=$false,
			Position=1)]
        [ValidateNotNullOrEmpty()]
        [string]
        $searchPattern,
        
        [Parameter(Mandatory=$true,
			ValueFromPipeline=$false,
			Position=2)]
        [ValidateNotNullOrEmpty()]
        [string]
        $stringToReplace,

        [Parameter(Mandatory=$true,
			ValueFromPipeline=$false,
			Position=3)]
        [ValidateNotNullOrEmpty()]
        [string]
        $replacement
    )

    Begin
    {
        Write-Verbose -Message "Starting Excel application."
        $ExcelApp = New-Object -ComObject Excel.Application
        
        $ExcelApp.Visible = $false
        $ExcelApp.DisplayAlerts = $false

        if(!(Test-Path .\excel_replacement_backup))
        {
            Write-Verbose -Message "Creating a folder to save each .xlsx file, purely for backup purposes."
            mkdir -Path .\ -Name "excel_replacement_backup"
        }
        "Touched files:" | Out-File -FilePath .\excel_replacement_backup\log.txt -Append
    }

    Process
    {
        Write-Verbose -Message "Copying backup of the file to .\excel_replacement_backup\"
        "$filepath is getting moved to the backup folder and then modified." | Out-File -FilePath .\excel_replacement_backup\log.txt -Append
        Copy-Item -Path $filepath -Destination .\excel_replacement_backup\

        Write-Verbose -Message "Opening the Excel File: $filepath"
        $workbook = $ExcelApp.Workbooks.Open($filepath)

        Write-Verbose -Message "Replacing the links matching the pattern"
        $workbook.LinkSources() | Where-Object {$_ -like $searchPattern } | % {$workbook.ChangeLink($_,($_ -replace $stringToReplace,$replacement),1)}
        
        Write-Verbose -Message "Saving and Exiting the File."
        $workbook.Save()
        $workbook.Close()
    }

    End
    {
        Write-Verbose -Message "Quitting the Excel application."
        $ExcelApp.quit()
    }
}
