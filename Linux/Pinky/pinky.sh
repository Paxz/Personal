﻿#!/bin/bash
REMOTE_IP="${SSH_CLIENT%% *}"
ADMIN_IP="192.168.0.41"
 
#falls die IP der Remote-Verbindung gleich der hier hinterlegten IP ist wird eine Benachrichtigung geschickt
if [ "$REMOTE_IP" != "$ADMIN_IP" ] ; then
  curl "https://api.telegram.org/botAPIKEY/sendMessage?chat_id=CHATID&text=Login auf $(hostname) am $(date +%Y-%m-%d) um $(date +%H:%M); Benutzer: $USER; $(pinky | tail -1)" &> /dev/null
fi