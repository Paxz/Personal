﻿#The file is red by the .bashrc file and is used to define aliases
#~/.bash_aliases

alias sudo='sudo '
alias update='sudo apt-get update'
alias upgrade='sudo apt-get upgrade'
alias doUpdates='sudo apt-get update && sudo apt-get upgrade'